exp : ne.eta (hole.arrow nat.nat (hole.arrow nat.nat nat.nat)).


exp_body : ne.eta (hole.arrow nat.nat (hole.arrow nat.nat nat.nat)).


eq_exp :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           logic.leibniz nat.nat (exp n m) (nat.filter_nat_type nat.nat (exp_body n) m)))).


def sym_eq_exp :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           logic.leibniz nat.nat (nat.filter_nat_type nat.nat (exp_body n) m) (exp n m))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  logic.sym_leibniz
    nat.nat
    (exp n m)
    (nat.filter_nat_type nat.nat (exp_body n) m)
    (eq_exp n m).


eq_exp_body_O :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) => logic.leibniz nat.nat (exp_body n nat.O) (nat.S nat.O))).


def sym_eq_exp_body_O :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) => logic.leibniz nat.nat (nat.S nat.O) (exp_body n nat.O)))
  :=
  n:(ne.eta nat.nat) =>
  logic.sym_leibniz nat.nat (exp_body n nat.O) (nat.S nat.O) (eq_exp_body_O n).


eq_exp_body_S :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           logic.leibniz nat.nat (exp_body n (nat.S m)) (nat.times (exp n m) n)))).


def sym_eq_exp_body_S :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           logic.leibniz nat.nat (nat.times (exp n m) n) (exp_body n (nat.S m)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  logic.sym_leibniz
    nat.nat
    (exp_body n (nat.S m))
    (nat.times (exp n m) n)
    (eq_exp_body_S n m).


def lt_O_exp :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) => ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O (exp n m)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  nat.nat_ind
    (_x_365:(ne.eta nat.nat) => ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O (exp n _x_365)))
    (sym_eq_exp
       n
       nat.O
       (y:(ne.eta nat.nat) => ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O y))
       (nat.sym_eq_filter_nat_type_O
          nat.nat
          (exp_body n)
          (y:(ne.eta nat.nat) => ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O y))
          (sym_eq_exp_body_O
             n
             (y:(ne.eta nat.nat) => ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O y))
             (auto:(ne.eps ne.i (nat.le (nat.S nat.O) n)) => nat.lt_O_S nat.O))))
    (a:(ne.eta nat.nat) =>
     sym_eq_exp
       n
       (nat.S a)
       (y:(ne.eta nat.nat) =>
        ne.imp
          ne.i
          (ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O (exp n a)))
          (ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O y)))
       (nat.sym_eq_filter_nat_type_S
          nat.nat
          (exp_body n)
          a
          (y:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O (exp n a)))
             (ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O y)))
          (sym_eq_exp_body_S
             n
             a
             (y:(ne.eta nat.nat) =>
              ne.imp
                ne.i
                (ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O (exp n a)))
                (ne.imp ne.i (nat.lt nat.O n) (nat.lt nat.O y)))
             (Hind:(ne.eps
                      ne.i
                      (ne.imp
                         ne.i
                         (nat.le (nat.S nat.O) n)
                         (nat.le (nat.S nat.O) (exp n a)))) =>
              posn:(ne.eps ne.i (nat.le (nat.S nat.O) n)) =>
              nat.eq_times_body_O
                (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                 nat.le (nat.S (y (nat.S nat.O))) (nat.times (exp n a) n))
                (nat.eq_filter_nat_type_O
                   (hole.arrow nat.nat nat.nat)
                   nat.times_body
                   (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                    nat.le (nat.S (y (nat.S nat.O))) (nat.times (exp n a) n))
                   (nat.eq_times
                      nat.O
                      (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                       nat.le (nat.S (y (nat.S nat.O))) (nat.times (exp n a) n))
                      (nat.eq_plus_body_O
                         (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                          nat.le
                            (nat.S (y (nat.times nat.O (nat.S nat.O))))
                            (nat.times (exp n a) n))
                         (nat.eq_filter_nat_type_O
                            (hole.arrow nat.nat nat.nat)
                            nat.plus_body
                            (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                             nat.le
                               (nat.S (y (nat.times nat.O (nat.S nat.O))))
                               (nat.times (exp n a) n))
                            (nat.eq_plus
                               nat.O
                               (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                                nat.le
                                  (nat.S (y (nat.times nat.O (nat.S nat.O))))
                                  (nat.times (exp n a) n))
                               (nat.eq_plus_body_S
                                  nat.O
                                  (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                                   nat.le
                                     (y (nat.times nat.O (nat.S nat.O)))
                                     (nat.times (exp n a) n))
                                  (nat.eq_filter_nat_type_S
                                     (hole.arrow nat.nat nat.nat)
                                     nat.plus_body
                                     nat.O
                                     (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                                      nat.le
                                        (y (nat.times nat.O (nat.S nat.O)))
                                        (nat.times (exp n a) n))
                                     (nat.eq_plus
                                        (nat.S nat.O)
                                        (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                                         nat.le
                                           (y (nat.times nat.O (nat.S nat.O)))
                                           (nat.times (exp n a) n))
                                        (nat.eq_times_body_S
                                           nat.O
                                           (y:(ne.eta (hole.arrow nat.nat nat.nat)) =>
                                            nat.le
                                              (y (nat.S nat.O))
                                              (nat.times (exp n a) n))
                                           (nat.eq_filter_nat_type_S
                                              (hole.arrow nat.nat nat.nat)
                                              nat.times_body
                                              nat.O
                                              (y:(ne.eta
                                                    (hole.arrow nat.nat nat.nat)) =>
                                               nat.le
                                                 (y (nat.S nat.O))
                                                 (nat.times (exp n a) n))
                                              (nat.eq_times
                                                 (nat.S nat.O)
                                                 (y:(ne.eta
                                                       (hole.arrow nat.nat nat.nat)) =>
                                                  nat.le
                                                    (y (nat.S nat.O))
                                                    (nat.times (exp n a) n))
                                                 (nat.le_times
                                                    (nat.S nat.O)
                                                    (exp n a)
                                                    (nat.S nat.O)
                                                    n
                                                    (Hind posn)
                                                    posn))))))))))))))))
    m.


