(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;       Defining the encoding of NE       ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

def set     : Type.
def eta     : set -> Type.
def preprop : Type.
def prop    : Type.

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;           Defining the indices          ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

Index        : Type.
i            : Index.
c            : Index.

def min      : Index -> Index -> Index.
[] min c _  --> c.
[] min _ c  --> c.
[] min i i  --> i.

def max      : Index -> Index -> Index.
[] max i _  --> i.
[] max _ i  --> i.
[] max c c  --> c.

Bool         : Type.
True         : Bool.
False        : Bool.

def conj               : Bool -> Bool -> Bool.
[] conj False _       --> False.
[] conj _     False   --> False.
[] conj True  True    --> True.

def leq      : Index -> Index -> Bool.
[] leq c c  --> True.
[] leq i i  --> True.
[] leq c i  --> True.
[] leq i c  --> False.

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;          Defining the embedding         ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

def eps      : Index -> preprop -> Type.

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;         Defining the connectives        ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

imp     : Index -> preprop -> preprop -> preprop.
and     : Index -> preprop -> preprop -> preprop.
true    : preprop.
false   : preprop.
not     : Index -> preprop -> preprop.
or      : Index -> preprop -> preprop -> preprop.
forall  : Index -> (s : set) -> ( (eta s) -> preprop ) -> preprop.
exists  : Index -> (s : set) -> ( (eta s) -> preprop ) -> preprop.
def not_c := A : preprop => imp c A false.

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;        Checking indice constraints      ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

def result_certif          : Bool -> preprop -> preprop.
[A] result_certif True  A --> A.
[]  result_certif False _ --> true.

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;            Introduction rules           ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

ax       : (iax : Index) -> (iconcl : Index) -> (A : preprop) ->
           (eps iax A) -> (eps iconcl (result_certif (leq iconcl iax) A)).
true_i   : (iT : Index) ->
           eps iT true.
and_i    : (iA : Index) -> (iB : Index) -> (iT : Index) -> (iand : Index) ->
           (A : preprop) -> (B : preprop) ->
           (eps iA A) -> (eps iB B) ->
           eps iT (result_certif (leq (min iT iand) (min iA iB)) (and iand A B)).
or_il    : (iA : Index) -> (iT : Index) -> (ior : Index) ->
           (A : preprop) -> (B : preprop) ->
           (eps iA A) ->
           eps iT (result_certif (leq (min iT ior) iA) (or ior A B)).
or_ir    : (iB : Index) -> (iT : Index) -> (ior : Index) ->
           (A : preprop) -> (B : preprop) ->
           (eps iB B) ->
           eps iT (result_certif (leq (min iT ior) iB) (or ior A B)).
imp_i    : (iA : Index) -> (iB : Index) -> (iT : Index) -> (iimp : Index) ->
           (A : preprop) -> (B : preprop) ->
           (eps iA A -> eps iB B) ->
           eps iT (result_certif (conj (leq (min iT iimp) iB) (leq iA iimp)) (imp iimp A B)).
exists_i : (iA : Index) -> (iT : Index) -> (iex : Index) ->
           (s : set) -> (t : eta s) -> (A : (eta s -> preprop)) ->
           (eps iA (A t)) ->
           eps iT (result_certif (leq (min iT iex) iA) (exists iex s A)).
forall_i : (iA : Index) -> (iT : Index) -> (ifa : Index) ->
           (s : set) -> (A : (eta s -> preprop)) ->
           (t : eta s -> eps iA (A t)) ->
           eps iT (result_certif (leq ifa iA) (forall ifa s A)).
neg_i    : (iA : Index) -> (ibot : Index) -> (iT : Index) -> (inot : Index) ->
           (A : preprop) ->
           (eps iA A -> eps ibot false) ->
           eps iT (result_certif (conj (leq (min iT inot) ibot) (leq iA ibot)) (not inot A)).

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;            Elimination rules            ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)

false_e : (iA : Index) -> (ibot : Index) ->
          (A : preprop) ->
          (eps ibot false) ->
          eps iA (result_certif (leq iA ibot) A).
and_el  : (iA : Index) -> (iT : Index) -> (iand : Index) ->
          (A : preprop) -> (B : preprop) ->
          eps iT (and iand A B) ->
          eps iA (result_certif (leq iA (min iT iand)) A).
and_er  : (iB : Index) -> (iT : Index) -> (iand : Index) ->
          (A : preprop) -> (B : preprop) ->
          eps iT (and iand A B) ->
          eps iB (result_certif (leq iB (min iT iand)) B).
imp_e   : (iA : Index) -> (iB : Index) -> (iT : Index) -> (iimp : Index) ->
          (A : preprop) -> (B : preprop) ->
          eps iT (imp iimp A B) -> eps iA A ->
          eps iB (result_certif (conj (leq iB (min iT iimp)) (leq iimp iA)) B).
or_e    : (iA : Index) -> (iB : Index) -> (iC : Index) -> (iT : Index) -> (ior : Index) ->
          (A : preprop) -> (B : preprop) -> (C : preprop) ->
          eps iT (or ior A B) -> (eps iA A -> eps iC C) -> (eps iB B -> eps iC C) ->
          eps iC (result_certif (leq (max iA (max iB iC)) (min iT ior)) C).
fa_e    : (iA : Index) -> (iT : Index) -> (ifa : Index) ->
          (s : set) -> (t : eta s) -> (A : (eta s -> preprop)) ->
          eps iT (forall ifa s A) ->
          eps iA (result_certif (leq iA (min iT ifa)) (A t)).
ex_e    : (iA : Index) -> (iB : Index) -> (iT : Index) -> (iex : Index) ->
          (s : set) -> (B : preprop) -> (A : (eta s -> preprop)) -> 
          eps iT (exists iex s A) -> ((t : eta s) -> eps iA (A t) -> eps iB B) ->
          eps iB (result_certif (leq (max iA iB) (min iT iex)) B).
neg_e	  : (iA : Index) -> (ibot : Index) -> (iT : Index) -> (inot : Index) ->
	        (A : preprop) ->
	        (eps iA A) -> (eps iT (not inot A)) ->
 	        eps ibot (result_certif (conj (leq ibot (min iT inot)) (leq inot iA)) false).

(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)
(;             Excluded Middle             ;)
(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;)


em : (iA : Index) -> (iT : Index) -> (ior : Index) -> (inot : Index) ->
     A : preprop ->
     eps iT (result_certif (leq iT c) (or ior A (not inot A))).
