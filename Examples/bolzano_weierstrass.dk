Set : Type.

A : Set.
B : Set.

F : Set -> ne.preprop.
U : Set -> Set -> Set.

def i := ne.i.
def c := ne.c.
def eps := ne.eps.

Finite_union :
  eps i
  (ne.imp c (ne.and i (F A) (F B)) (F (U A B))).

def FUAB := (F (U A B)).
def nFAnFB := (ne.or i (ne.not i (F A)) (ne.not i (F B))).

thm BW :
  ne.eps i
  (ne.imp c (ne.not c FUAB) nFAnFB) :=
  ne.imp_i c c i c
    (ne.not c (F (U A B)))
    (ne.or i (ne.not i (F A)) (ne.not i (F B)))
    (xnfuab : eps c (ne.not c (F (U A B))) =>
      ne.or_e c c c c i
      (F A)
      (ne.not i (F A))
      nFAnFB
      (ne.em i c i i (F A))
      (xfa : eps c (F A) =>
        (ne.or_e c c c c i
          (F B)
          (ne.not i (F B))
          nFAnFB
          (ne.em i c i i (F B))
          (xfb : eps c (F B) =>
               ne.false_e c c nFAnFB
                 (ne.neg_e c c c c FUAB
                   (ne.imp_e c c i c
                     (ne.and i (F A) (F B))
                     FUAB
                     Finite_union
                     (ne.and_i c c c i (F A) (F B) xfa xfb))
                     xnfuab))
          (xfb : eps c (ne.not i (F B)) =>
            ne.or_ir c c i (ne.not i (F A)) (ne.not i (F B)) xfb)))
      (xfa : eps c (ne.not i (F A)) => ne.or_il c c i (ne.not i (F A)) (ne.not i (F B)) xfa)).
